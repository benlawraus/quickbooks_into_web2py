# -*- coding: utf-8 -*-
"Copy Quickbooks data to remote postgresql database"
import sys
import pyodbc
import json
import pprint
import traceback
W_PREFIX = "qb_"
DEBUG = True
def get_table_name(table_name):
    web2py_table_name = W_PREFIX + table_name
    return web2py_table_name
def fieldrename(field):
    RESERVED = ["id","desc","type","initial","empty","value","sin","location"\
        ,"maximum","minimum","indicator"]
    f = field.lower()
    if f in RESERVED:
        return W_PREFIX+f
    else:
        return field

def query(conn, sql, *args):
    "Execute a SQL query and return rows as a list of dicts"
    cur = conn.cursor()
    ret = []
    try:
        if DEBUG: 
            print >> sys.stderr, "QUERY:\n", sql % args
        cur.execute(sql % args)

        for row in cur:
            dic = {}
            for i, value in enumerate(row):
                field = cur.description[i][0]
                dic[field] = value
            if DEBUG: print >> sys.stderr, "RET: ", dic
            ret.append(dic)
        return ret
    finally:
        cur.close()


def print_error(w):
    debug_str = pprint.pformat(w)
    return

def raise_error(w, cursor, conn, msg=""):
    cursor.close()
    conn.close()
    print_error(w)
    raise RuntimeError(msg+debug_str)
    return 0


def get_reference_record(field_info, parent_value, web2py_fields, cursor, conn):
    # find the record it is referencing
    parent_table_ref = field_info['type'][len("reference " + W_PREFIX):]
    orig_parent_value = parent_value
    dal_child_table = W_PREFIX + parent_table_ref
    # find record reference table and record
    for child_w in web2py_fields:
        if parent_table_ref == child_w['table_name']:
            for child_f in child_w['fields']:
                # find primary key
                if child_f['extra'] and (child_f['extra']\
                        .find("unique = True") > (-1)):
                    # find corresponding record
                    try:
                        # is the primary key of the child a reference ?!?
                        if child_f['type'].find("reference") > -1:
                            # it is a reference too!
                            row = get_reference_record(child_f, parent_value\
                                , web2py_fields, cursor, conn)
                            parent_value = row.id
                        child_rows = db( db[dal_child_table]\
                            [ fieldrename(child_f['name']) ] ==  parent_value)\
                            .select()
                    except:
                        # the primary key should NOT be a reference!
                        # there should NOT be more than ONE primary key!
                        # search the whole table
                        raise_error(child_f, cursor, conn, "Cannot fit {0}"\
                            " into {1} of {2}".format(orig_parent_value\
                                , fieldrename(child_f['name'])\
                                , child_w['table_name']))
                    if (child_rows is None) or len(child_rows) == 0:
                        # create a record
                        new_dict = {fieldrename(child_f['name']) : parent_value\
                            , 'in_qb' : 'False'}
                        row = db[dal_child_table].insert(**new_dict)
                        db.commit()
                        if DEBUG:
                            print("Cannot find reference for {0}"\
                                .format(child_f['name']))
                            print("Original value {0} in {1}"\
                                .format(orig_parent_value,parent_table_ref))
                            print_error(new_dict)
                        return row
                    elif len(child_rows) == 1:
                        row = child_rows.first()
                        return row
                    else:
                        row = child_rows.last()
                        if DEBUG:
                            print("Too many references for {0}".format(child_f['name']))
                        return row
                    break
            break
    raise_error(child_f, cursor, conn, "Cannot find reference table")
    return None

def sync_database(conn, table_info, db, web2py_fields,from_date):
    # sync the first table
    # get records one by one
    cursor = conn.cursor()
    if from_date == 'all':
        sql_str = "select * from " + table_info['table_name']
    cursor.execute(sql_str)
    list_of_fields = [f['name'] for f in table_info['fields']]
    w2p_table_name = get_table_name(table_info['table_name'])
    try:
        qb_row = cursor.fetchone()
    except:
        print("TABLE {0} ERROR ".format(table_info['table_name']))
        print("NOT LOADING TABLE")
        cursor.close()
        traceback.print_exc()
        return

    # build list of column names to use as dictionary keys from sql results
    columns = [column[0] for column in cursor.description]
        
    while qb_row is not None:
        row_dict = {}
        prim_key = False
        row = None
        for i,value in enumerate(qb_row):
            field = columns[i]
            # does it exist in web2py table definition?
            if field not in list_of_fields:
                raise_error(w, cursor, conn, msg=\
                    "QB field {0} not in web2py fields for "\
                    "table {1}".format(field,table_info['table_name']))
            row_dict[field] = value
            for f in table_info['fields']:
                if f['name'] == field:
                    # is the field a reference?
                    if f['type'].find("reference") > -1:
                        ref_row = get_reference_record(f, row_dict[field] \
                            , web2py_fields, cursor, conn)
                        row_dict[field] = ref_row.id
                    # is field the primary key?
                    if f['extra'] and (f['extra'].find("unique = True") > (-1)):
                        # use this to find corresponding record in DAL
                        prim_key = True
                        try:
                            rows = db( db[w2p_table_name]\
                                [fieldrename(f['name'])] == row_dict[field] )\
                                .select()
                        except:
                            raise_error(row_dict,cursor,conn,"Error for "\
                                "field {0} with {1} = {2}"\
                                .format(fieldrename(f['name']),field,value))
                        if len(rows) > 1:
                            raise_error(row_dict,cursor,conn,"Too many rows for "\
                                "table {0} with {1} = {2}"\
                                .format(table_info['table_name'],field,value))
                        elif rows is not None:
                            row = rows.first()
        row_dict['in_qb'] = 'True'
        if prim_key:
            if row is None: #insert into database
                try:
                    record = db[w2p_table_name].insert(**row_dict)
                except:
                    # cannot insert so modify it TODO
                    row_dict['table_name'] = table_name
                    raise_error(row_dict, cursor, conn, "CANNOT INSERT")
            else:
                try:
                    db(db[w2p_table_name]._id == row.id).update(**row_dict)
                except:
                    row_dict['table_name'] = table_info['table_name']
                    raise_error(row_dict, cursor, conn)
                # check if they are the same
        else:
            raise_error(w,cursor,conn,"No primary key for "\
                "table {0}".format(table_info['table_name']))
        db.commit()
        try:
            qb_row = cursor.fetchone()
        except:
            print("TABLE {0} ERROR ".format(table_info['table_name']))
            print("NOT LOADING TABLE")
            traceback.print_exc()
            break
    if DEBUG:
        print("TABLE {0} COMPLETE".format(table_info['table_name']))
    cursor.close()
    return


def sync(from_date = 'all'):
    DSNconnection='DSN=qbconnection;'

    file_web2py_fields = "web2py_fields.json"
    conn = pyodbc.connect(DSNconnection, autocommit=True)
    
    web2py_fields = json.load(open(file_web2py_fields,"r"))
    for table_info in web2py_fields:
        if DEBUG:
            print("TABLE {0}".format(table_info['table_name']))

        sync_database(conn, table_info, db, web2py_fields, from_date)

    conn.close()
    return dict()

