# quickbooks_into_web2py
There are two files:

1. ```extract_quickbooks_models.py```

2. ```qb_postgres_sync.py```


The quickbooks company file has to be open and and QODBC running. The latter is free software with quickbooks desktop. See

[https://enterprisesuite.intuit.com/customer-resource-center/odbc-drivers/]

and 

[https://qodbc.com/connecting-to-quickbooks/]


```extract_quickbooks_models.py``` needs to run in a python win32 bit environment. You would have downloaded the ```py.exe``` file with the windows installation of *web2py*. Please use this.

Place ```extract_quickbooks_models.py``` in the ```modules``` directory of your *web2py* app because ```qb_postgres_sync.py``` needs a function in that file. 
Once you run ```extract_quickbooks_models.py```, it will generate some json files that describe the quickbooks schema. These are also needed by ```qb_postgres_sync.py```.
It will also generate a file called ```db_define_tables.py``` which is a *web2py* file. Make sure this is in your models directory too.


Once the tables are defined, use ```qb_postgres_sync.py``` to copy the quickbooks data into your database.

Run it like this

```
 ../../web2py.exe -M -S qb/qb_postgres_sync/sync
 ```
 
 where ```qb``` is the name of your *web2py* app.
 
 This is work in progress.



