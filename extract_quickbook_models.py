# -*- coding: utf-8 -*-
import traceback
import json

DEFINE_TABLE_FILE = "applications/qb/models/define_tables.py"
HELP = """
USAGE: extract web2py models from Quickbooks data.
REQUIREMENTS: qodbc for windows and set up with quickbooks desktop
RUN: from the main web2py directory with the py.exe 32-bit python.
OUTPUT:
1. json files describing the tables, the fields and the web2py table 
        definitions and the references. 
2. {0}
""".format(DEFINE_TABLE_FILE)

print(HELP)
# Config options
DEBUG = True  # print debug messages to STDERR
SCHEMA = 'dbo'
COMMAND_LINE_MODE = True  # running from command prompt. Disable to specify variables and use in IDE
# Only specify values below if not running from command line
DSNconnection='DSN=qbconnection;'

# Constant for Field keyword parameter order (and filter):
KWARGS = ('type', 'length', 'default', 'required', 'ondelete',
          'notnull', 'unique', 'label', 'comment', 'rname')

import sys
import re
# This is from pydal/helpers/regex.py as of 2016-06-16
# Use this to recognize if a field name need to have an rname representation
REGEX_VALID_TB_FLD = re.compile(r'^[^\d_][_0-9a-zA-Z]*\Z')
# For replacing invalid characters in field names
INVALID_CHARS = re.compile(r'[^a-zA-Z0-9_]')
W_PREFIX = "qb_"



def get_tables(conn, schema=SCHEMA):
    "List table names in a given schema"
    try:
        table_list = json.load(open("tables.json"))
    except:
        rows = conn.execute("SP_TABLES")
        table_list = []
        for row in rows:
            if DEBUG:
                print(row)
            table_list.append(dict(name=row[2],description=row[4]))
        json.dump(table_list,open("tables.json","w"))
    return table_list

def primarykeys(cursor, table):
    "Find primary keys"
    primary_keys = []
    rows = cursor.execute("SP_PRIMARYKEYS {0}".format(table['name'])).fetchall()
    for row in rows:
        primary_keys.append(row[3])
    return primary_keys

def get_fields(cursor, table):
    "Retrieve field list for a given table"
    rows = cursor.execute('SP_COLUMNS {0}'.format(table['name'])).fetchall()
    primary_keys = primarykeys(cursor, table)
    fields=[]
    for row in rows:
        fields.append(dict(
            column_name = row[3]
            , data_type = row[5] 
            , character_maximum_length = row[7]
            , column_default = row[12]
            , is_nullable = row[10]
            , unique = (row[3] in primary_keys) ))
    return fields


def define_field(table, field, refs):
    "Determine field type, default value, references, etc."
    f = dict(name = '', type = '')
    # is field a reference?
    for ref in refs:
        if ref[0]['table_name'] == table['name']:
            for r in ref:
                if r['data_type'] is not None:
                    if field['column_name'].lower() == r['field_name'].lower():
                        f['type'] = r['data_type']
                        break
    f['name'] = field['column_name']
    data_type = field['data_type'].lower()
    if f['type'] != '':
        pass
    elif data_type.startswith('character'):
        f['type'] = "string"
        if field['character_maximum_length']:
            f['length'] = field['character_maximum_length']
    elif data_type in ('text', 'ntext'):
        f['type'] = "text"
    elif data_type in ('boolean', 'bit'):
        f['type'] = "boolean"
    elif data_type in ('tinyint', 'smallint', 'bigint', 'int', 'integer'):
        f['type'] = "integer"
    elif data_type in ('real', 'float','double'):
        f['type'] = "double"
    elif data_type in ('datetime', 'datetime2', 'smalldatetime'):
        f['type'] = "datetime"
    elif data_type in ('timestamp',):
        f['type'] = "datetime"
    elif data_type in ('date',):
        f['type'] = "date"
    elif data_type in ('time',):
        f['type'] = "time"
    elif data_type in ('numeric', 'money', 'smallmoney', 'decimal'):
        f['type'] = "double"
    elif data_type in ('binary', 'varbinary', 'image'):
        f['type'] = "blob"
    elif data_type in ('varchar', 'char', 'nchar', 'nvarchar', 'uniqueidentifer'):
        f['type'] = "string"
    else:
        raise RuntimeError("Data Type not supported: %s " % str(field))
    field_lower = f['name'].lower()
    if (field_lower.find("memo") > -1) or (field_lower.find("desc") > -1):
        if f['type'] == "string":
            f['type'] = "text"

    try:
        if field['column_default']:
            if field['column_default'].lower() in ('true',1):
                d = "True"
            elif field['column_default'].lower() in ('false',0):
                d = "False"
            elif field['column_default'].lower() == 'null':
                d = None
            else:
                d = field['column_default']
            f['default'] = d
    except (ValueError, SyntaxError):
        pass
    except Exception as e:
        raise RuntimeError("Default unsupported '%s" % field['column_default'])
    if not field['is_nullable']:
        f['extra'] = "requires = IS_NOT_EMPTY()"
    else:
        f['extra'] = None

    if field['unique']:
        if f['extra']:
            f['extra'] += ", unique = True"
        else:
            f['extra'] = "unique = True"
    return f


def is_unique(conn, table, field):
    "Find unique columns (incomplete support)"
    return

def fieldrename(field):
    RESERVED = ["id","desc","type","initial","empty","value","sin","location" \
        ,"maximum","minimum","indicator"]
    f = field.lower()
    if f in RESERVED:
        return W_PREFIX+f
    else:
        return field

def references(conn, table1, tables):
    "Find a FK"
    reference = []
    for table2 in tables:
        rows_cursor = conn.execute("SP_FOREIGNKEYS {0} {1}".format(table2['name']\
            ,table1['name']))
        rows = rows_cursor.fetchall()
        if len(rows) == 0:
            ref = dict(table_name = table1['name'], field_name = None \
                , data_type = None \
                , ref_table = table2['name'] )
            reference.append(ref)
            if DEBUG:
                print("name {0} data_type {1} table 2 {2}"\
                    .format(ref['table_name'],ref['field_name'],ref['ref_table']))
        else:
            for row in rows:
                ref = dict(table_name = table1['name'], field_name = row[7] \
                    , data_type = "reference {0}".format(W_PREFIX + table2['name']) \
                    , ref_table = table2['name'] )
                if DEBUG:
                    print("name {0} data_type {1}".format(ref['table_name'],ref['field_name']))
                reference.append(ref)
    return reference

def get_table_name(table_name):
    web2py_table_name = W_PREFIX + table_name
    return web2py_table_name

def define_table_file(fp, table, tables, refs, fields):
    "Output single table definition"
    web2py_table_name = get_table_name(table['name'])
    str_cmd =  "    db.define_table('{0}'\n".format(web2py_table_name)
    for field in fields:
        fdef = define_field(table, field, refs)
        str_cmd += "    , Field('{0}', type = '{1}' "\
            .format(fieldrename(fdef['name']), fdef['type'])
        if fdef['extra']:
            str_cmd += ", {0}".format(fdef['extra'])
        str_cmd += " )\n"
    # this is in qb
    str_cmd += "    , Field('in_qb', type='string') )\n\n"
    fp.write(str_cmd)
    return

def define_table_json(table, tables, refs, fields):
    "Output single table definition in json file"
    str_cmd = dict()
    str_cmd['table_name'] = table['name']
    str_cmd['fields'] = []
    for field in fields:
        fdef = define_field(table, field, refs)
        str_cmd['fields'].append(fdef)
    str_cmd['fields'].append(dict(name = 'in_qb', type = 'string'))
    return str_cmd



def filter_table(table_name,new_table,left_over,reference):
    # are all the referenced tables already in new_table?
    add_into_table = True
    for r in reference:    
        if r['field_name'] is not None:
            if r['ref_table'] != r['table_name']:# check for self-reference
                if r['ref_table'] not in new_table:
                    # referenced table is not in new table so you cannot put
                    # r['table_name'] in new_table just yet
                    add_into_table = False
    if add_into_table:
        if table_name in left_over:
            left_over.remove(table_name)
        if table_name not in new_table:
            new_table.append(table_name)
    else: # keep it in left over
        if table_name not in left_over:
            left_over.append(table_name)

    if DEBUG:
        print("len(table_name)={0} len(left_over)={1}".format(len(table_name),len(left_over)))
    return

def get_references(cursor, tables):
    try:
        refs = json.load(open("reference.json"))
        if DEBUG:
            print("length of refs is {0}".format(len(refs)))
    except:
        refs=[]
        for table in tables:
            reference = references(cursor, table, tables)
            refs.append(reference)
        json.dump(refs,open("reference.json","w"))
    return refs

def define_db(conn):

    "Output database definition (model)"
    # First get all the information from quickbooks
    with conn.cursor() as cursor:
        tables = get_tables(cursor)
        # does the references exist in a file?
        refs = get_references(cursor, tables)

    # need to order the tables first
    new_table=[]
    left_over=[]
    #   set up the two lists new_table and left_over
    for table in tables:
        for reference in refs:
            if reference[0]['table_name'] == table['name']:
                filter_table(table['name'],new_table,left_over,reference)
                break
    # reorder tables
    ix = 0
    checker = 0
    MAX_checker = 1000
    while (len(left_over)>0) and (checker<MAX_checker):
        checker += 1
        if DEBUG:
            print("Iteration {0}, len(left_over)={1}, len(new_table)={2}"\
                .format(checker,len(left_over),len(new_table)))
        table_name = left_over[ix]
        # find references for this table
        for reference in refs:
            if reference[0]['table_name'] == table_name:
                filter_table(table_name,new_table,left_over,reference)
                break
        if ix+1 < len(left_over):
            ix += 1
        else:
            ix = 0
    if checker==MAX_checker or checker==0:
        print("Not all tables in new_table")
        for l in left_over:
            print(l)

        cnn.close()
        raise RuntimeError

    # define table in new order
    web2py_fields = []
    with open(DEFINE_TABLE_FILE,"w") as fp:
        def_string = """# -*- coding: utf-8 -*-
def db_define_table(db):
"""

        with conn.cursor() as cursor:
            try:
                all_fields = json.load(open("fields.json"))
            except:
                all_fields=[]
                for table in new_table:
                    t = dict(name = table)
                    fields = get_fields(cursor, t)
                    all_fields.append(fields)
                json.dump(all_fields,open("fields.json","w"))

        fp.write(def_string)
        for ix,table in enumerate(new_table):
            t = dict(name = table)
            define_table_file(fp, t, tables, refs, all_fields[ix])
            def_fields = define_table_json(t, tables, refs, all_fields[ix])
            web2py_fields.append(def_fields)
    json.dump(web2py_fields,open("web2py_fields.json","w"))
    return


if __name__ == "__main__":
    if True:
        # Make the database connection (change driver if required)
        import pyodbc
        # cnn = pyodbc.connect(database=db, host=host, port=port,
        #                        user=user, password=passwd,
        #                        )
        cnn = pyodbc.connect(DSNconnection, autocommit=True)
        # Start model code generation:
        define_db(cnn)
        cnn.close()
